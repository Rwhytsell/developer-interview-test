﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Smartwyre.DeveloperTest.Data;
using Smartwyre.DeveloperTest.Services;

namespace Smartwyre.DeveloperTest.Runner
{
    class Program
    {
        static Task Main(string[] args)
        {
            using var host = CreateHostBuilder(args).Build();
            return host.StartAsync();
        }
        
        static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((_, services) =>
                    services
                        .AddHostedService<Worker>()
                        .AddScoped<IAccountDataStore, AccountDataStore>()
                        .AddScoped<IPaymentService, PaymentService>());
    }
}
