using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Smartwyre.DeveloperTest.Services;
using Smartwyre.DeveloperTest.Types;

namespace Smartwyre.DeveloperTest.Runner
{
    public class Worker : BackgroundService
    {
        private readonly IHostLifetime _lifetime;
        private readonly IPaymentService _paymentService;

        public Worker(IHostLifetime lifetime, IPaymentService paymentService)
        {
            _lifetime = lifetime;
            _paymentService = paymentService;
        }
        
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            Console.WriteLine("Started executing");
            var result = _paymentService.MakePayment(new MakePaymentRequest()
            {
                DebtorAccountNumber = "1234",
                Amount = new decimal(42.00),
                PaymentDate = DateTime.Now,
                PaymentScheme = PaymentSchemes.BankToBankTransfer
            });
            Console.WriteLine("Execution ending " + (result.Success ? "Task was successful" : "Task failed"));
            return _lifetime.StopAsync(stoppingToken);
        }
    }
}