﻿namespace Smartwyre.DeveloperTest.Types
{
    public class Account
    {
        public string AccountNumber { get; set; }
        public decimal Balance { get; set; }
        public AccountStatus Status { get; set; }
        public PaymentSchemes AllowedPaymentSchemes { get; set; }
    }
}
