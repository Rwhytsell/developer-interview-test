﻿using System;
using Smartwyre.DeveloperTest.Data;
using Smartwyre.DeveloperTest.Types;

namespace Smartwyre.DeveloperTest.Services
{
    public class PaymentService : IPaymentService
    {
        private readonly IAccountDataStore _accountDataStore;
        public PaymentService(IAccountDataStore accountDataStore)
        {
            _accountDataStore = accountDataStore;
        }
        public MakePaymentResult MakePayment(MakePaymentRequest request)
        {
            var account = _accountDataStore.GetAccount(request.DebtorAccountNumber);
            
            var result = new MakePaymentResult
            {
                Success = IsPaymentSchemeAllowed(account, request)
            };

            if (!result.Success) return result;
            
            account.Balance -= request.Amount;
            _accountDataStore.UpdateAccount(account);

            return result;
        }

        private static bool IsPaymentSchemeAllowed(Account account, MakePaymentRequest request)
        {
            if (account == null)
            {
                return false;
            }
            
            switch (request.PaymentScheme)
            { 
                case PaymentSchemes.BankToBankTransfer:
                    break;
                case PaymentSchemes.ExpeditedPayments:
                    if (account.Balance < request.Amount)
                    {
                        return false;
                    }
                    break;
                case PaymentSchemes.AutomatedPaymentSystem: 
                    if (account.Status != AccountStatus.Live)
                    {
                        return false;
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(request),"Payment Scheme was not expected.");
            }
            return account.AllowedPaymentSchemes.HasFlag(request.PaymentScheme);
        }
    }
}
