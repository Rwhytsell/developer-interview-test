using System;
using Moq;
using Smartwyre.DeveloperTest.Data;
using Smartwyre.DeveloperTest.Services;
using Smartwyre.DeveloperTest.Types;
using Xunit;

namespace Smartwyre.DeveloperTest.Tests
{
    public class PaymentServiceTests
    {
        private readonly Mock<IAccountDataStore> _accountDataStoreMock = new Mock<IAccountDataStore>();

        private readonly Account _defaultAccount = new()
        {
            AccountNumber = "123abc",
            AllowedPaymentSchemes = PaymentSchemes.BankToBankTransfer,
            Balance = new decimal(10.00),
            Status = AccountStatus.Live
        };
        
        private  readonly MakePaymentRequest _defaultPaymentRequest = new()
        {
            DebtorAccountNumber = "123abc",
            Amount = new decimal(5.01),
            PaymentDate = DateTime.Now,
            PaymentScheme = PaymentSchemes.BankToBankTransfer,
        };
        
        [Fact]
        public void MakePayment_HappyPath()
        {
            var testAccount = _defaultAccount;
            var paymentRequest = _defaultPaymentRequest;
            _accountDataStoreMock.Setup(a => a.UpdateAccount(It.IsAny<Account>()));
            _accountDataStoreMock.Setup(a => a.GetAccount(It.IsAny<string>()))
                .Returns(testAccount);
            
                
            var paymentService = new PaymentService(_accountDataStoreMock.Object);

            var result = paymentService.MakePayment(paymentRequest);
            
            Assert.True(result.Success);
            _accountDataStoreMock.Verify(mock => mock.UpdateAccount(It.IsAny<Account>()), Times.Once());
        }
        
        [Fact]
        public void MakePayment_CompositEnum()
        {
            var testAccount = _defaultAccount;
            testAccount.AllowedPaymentSchemes = PaymentSchemes.ExpeditedPayments | PaymentSchemes.BankToBankTransfer;
            var paymentRequest = _defaultPaymentRequest;
            paymentRequest.PaymentScheme = PaymentSchemes.BankToBankTransfer;
            
            _accountDataStoreMock.Setup(a => a.UpdateAccount(It.IsAny<Account>()));
            _accountDataStoreMock.Setup(a => a.GetAccount(It.IsAny<string>()))
                .Returns(testAccount);

            var paymentService = new PaymentService(_accountDataStoreMock.Object);

            var result = paymentService.MakePayment(paymentRequest);
            
            Assert.True(result.Success);
            _accountDataStoreMock.Verify(mock => mock.UpdateAccount(It.IsAny<Account>()), Times.Once());
        }
        [Fact]
        public void MakePayment_SchemeNotAllowed()
        {
            var testAccount = _defaultAccount;
            testAccount.AllowedPaymentSchemes = PaymentSchemes.BankToBankTransfer;
            var paymentRequest = _defaultPaymentRequest;
            paymentRequest.PaymentScheme = PaymentSchemes.ExpeditedPayments;
            _accountDataStoreMock.Setup(a => a.UpdateAccount(It.IsAny<Account>()));
            _accountDataStoreMock.Setup(a => a.GetAccount(It.IsAny<string>()))
                .Returns(testAccount);

            var paymentService = new PaymentService(_accountDataStoreMock.Object);

            var result = paymentService.MakePayment(paymentRequest);
            
            Assert.False(result.Success);
            _accountDataStoreMock.Verify(mock => mock.UpdateAccount(It.IsAny<Account>()), Times.Never());
        }
        
        [Fact]
        public void MakePayment_CompositeEnum_SchemeNotAllowed()
        {
            var testAccount = _defaultAccount;
            testAccount.AllowedPaymentSchemes = PaymentSchemes.BankToBankTransfer | PaymentSchemes.ExpeditedPayments;
            var paymentRequest = _defaultPaymentRequest;
            paymentRequest.PaymentScheme = PaymentSchemes.AutomatedPaymentSystem;
            _accountDataStoreMock.Setup(a => a.UpdateAccount(It.IsAny<Account>()));
            _accountDataStoreMock.Setup(a => a.GetAccount(It.IsAny<string>()))
                .Returns(testAccount);

            var paymentService = new PaymentService(_accountDataStoreMock.Object);

            var result = paymentService.MakePayment(paymentRequest);
            
            Assert.False(result.Success);
            _accountDataStoreMock.Verify(mock => mock.UpdateAccount(It.IsAny<Account>()), Times.Never());
        }
        
        [Fact]
        public void MakePayment_ExpeditedPayment_InsufficientBalance()
        {
            var testAccount = _defaultAccount;
            testAccount.AllowedPaymentSchemes = PaymentSchemes.ExpeditedPayments;
            testAccount.Balance = new decimal(1.50);
            var paymentRequest = _defaultPaymentRequest;
            paymentRequest.PaymentScheme = PaymentSchemes.ExpeditedPayments;
            paymentRequest.Amount = new decimal(5.00);
            _accountDataStoreMock.Setup(a => a.UpdateAccount(It.IsAny<Account>()));
            _accountDataStoreMock.Setup(a => a.GetAccount(It.IsAny<string>()))
                .Returns(testAccount);

            var paymentService = new PaymentService(_accountDataStoreMock.Object);

            var result = paymentService.MakePayment(paymentRequest);
            
            Assert.False(result.Success);
            _accountDataStoreMock.Verify(mock => mock.UpdateAccount(It.IsAny<Account>()), Times.Never());
        }
        
        [Fact]
        public void MakePayment_AutomatedPayments_NonLiveAccount()
        {
            var testAccount = _defaultAccount;
            testAccount.AllowedPaymentSchemes = PaymentSchemes.AutomatedPaymentSystem;
            testAccount.Status = AccountStatus.Disabled;
            var paymentRequest = _defaultPaymentRequest;
            paymentRequest.PaymentScheme = PaymentSchemes.AutomatedPaymentSystem;
            _accountDataStoreMock.Setup(a => a.UpdateAccount(It.IsAny<Account>()));
            _accountDataStoreMock.Setup(a => a.GetAccount(It.IsAny<string>()))
                .Returns(testAccount);

            var paymentService = new PaymentService(_accountDataStoreMock.Object);

            var result = paymentService.MakePayment(paymentRequest);
            
            Assert.False(result.Success);
            _accountDataStoreMock.Verify(mock => mock.UpdateAccount(It.IsAny<Account>()), Times.Never());
        }
        
        [Fact]
        public void MakePayment_InvalidPaymentScheme_ThrowsException()
        {
            var testAccount = _defaultAccount;
            var paymentRequest = _defaultPaymentRequest;
            paymentRequest.PaymentScheme = (PaymentSchemes)(1 << 3);
            _accountDataStoreMock.Setup(a => a.UpdateAccount(It.IsAny<Account>()));
            _accountDataStoreMock.Setup(a => a.GetAccount(It.IsAny<string>()))
                .Returns(testAccount);

            var paymentService = new PaymentService(_accountDataStoreMock.Object);

            Assert.Throws<ArgumentOutOfRangeException>(() => paymentService.MakePayment(paymentRequest));
        }
        
        [Fact]
        public void MakePayment_NullAccount()
        {
            var paymentRequest = _defaultPaymentRequest;
            _accountDataStoreMock.Setup(a => a.UpdateAccount(It.IsAny<Account>()));
            _accountDataStoreMock.Setup(a => a.GetAccount(It.IsAny<string>()))
                .Returns((Account)null);

            var paymentService = new PaymentService(_accountDataStoreMock.Object);

            var result = paymentService.MakePayment(paymentRequest);
            
            Assert.False(result.Success);
        }
    }
}
